const { UserModel } = require('../../models/user')
const { PersonalAccessTokenModel } = require('../../models/personalAccessToken')
const responseHandlers = require('../utils/response')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

const _register = async (req, res, next) => {
  try {
    const { first_name, last_name, username, password, role } = req.body
    const checkUser = await UserModel.findOne({ username })

    if (checkUser) return responseHandlers.conflict(res, 'username นี้มีอยู่ในระบบแล้ว')

    const hashPassword = await bcrypt.hash(password, 10)
    const userObj = { first_name, last_name, username, password: hashPassword, role }
    const user = await UserModel.create(userObj)

    return responseHandlers.created(res, user, 'Register Success')
  } catch (error) {
    return responseHandlers.catchError(res, error)
  }
}

const _login = async (req, res, next) => {
  try {
    const { username, password } = req.body
    const user = await UserModel.findOne({ username })

    if (!user) return responseHandlers.notFound(res, 'ไม่พบอีเมลล์นี้ในระบบ')

    const checkPassword = await bcrypt.compare(password, user.password)

    if (!checkPassword) return responseHandlers.badRequest(res, 'รหัสผ่านไม่ถูกต้อง')

    const secretKey = process.env.APP_KEY
    const tokenGenerated = jwt.sign({ ...user }, secretKey)
    const tokenObj = { user, token: tokenGenerated }
    const token = await PersonalAccessTokenModel.findOne({ user: user._id, expired: false })

    if (token) {
      await PersonalAccessTokenModel.findByIdAndUpdate(token.id, { ...token._doc, expired: true })
      jwt.verify(token.token, process.env.APP_KEY)
    }

    const createdPersonalAccessToken = await PersonalAccessTokenModel.create(tokenObj)
    const findPersonalAccessToken = await PersonalAccessTokenModel.findById({ _id: createdPersonalAccessToken._id }).populate([
      { path: 'user', select: '-password' }
    ])

    return responseHandlers.success(res, findPersonalAccessToken)
  } catch (error) {
    return responseHandlers.catchError(res, error)
  }
}

const _logout = async (req, res, next) => {
  try {
    const token = req.headers.authorization?.split(' ')[1]

    if (!token) return responseHandlers.unAuthorized(res)

    jwt.verify(token, process.env.APP_KEY)
    await PersonalAccessTokenModel.findOneAndUpdate({ token }, { expired: true })

    return responseHandlers.success(res)
  } catch (error) {
    return responseHandlers.catchError(res, error)
  }
}

const _checkToken = async (req, res, next) => {
  try {
    const personalAccessToken = await PersonalAccessTokenModel.find({
      _id: req.body._id,
      token: req.body.token,
      user: req.body.user._id,
      expired: false
    })
    jwt.verify(req.body.token, process.env.APP_KEY)

    return responseHandlers.success(res, personalAccessToken.length > 0)
  } catch (error) {
    return responseHandlers.catchError(res, error)
  }
}

module.exports = {
  _register,
  _login,
  _logout,
  _checkToken
}