module.exports = {
  success: 'SUCCESS',
  created: 'CREATED',
  badRequest: 'BAD_REQUEST',
  unAuthorized: 'UNAUTHORIZED',
  notFound: 'NOT_FOUND',
  conflict: 'CONFLICT',
  internalServerError: 'INTERNAL_SERVER_ERROR',
  failure: 'FAILURE',
  recordNotFound: 'RECORD_NOT_FOUND',
  validationError: 'VALIDATION_ERROR'
}