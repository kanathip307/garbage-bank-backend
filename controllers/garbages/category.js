const { GarbageCategoryModel } = require('../../models/garbages/category')
const { GarbageModel } = require('../../models/garbages')
const responseHandlers = require('../utils/response')

const _find = async (req, res, next) => {
  try {
    // ดึง garbage_category ที่ผูกอยู่กับ garbages
    const garbageCategories = await GarbageCategoryModel.find().populate([
      { path: 'garbages', select: '-garbage_category -garbages' } // ใส่ขีด - ด้านหน้าฟิลด์ที่ไม่ต้องการให้ดึงมาด้วย
    ])

    return responseHandlers.success(res, garbageCategories)
  } catch (error) {
    return responseHandlers.catchError(res, error)
  }
}

const _findByID = async (req, res, next) => {
  try {
    const garbageCategory = await GarbageCategoryModel.findById(req.params.id).populate([
      { path: 'garbages', select: '-garbage_category -garbages' } // ใส่ขีด - ด้านหน้าฟิลด์ที่ไม่ต้องการให้ดึงมาด้วย
    ])

    if (!garbageCategory) return responseHandlers.notFound(res, 'ไม่พบหมวดหมู่ของขยะ')

    return responseHandlers.success(res, garbageCategory)
  } catch (error) {
    return responseHandlers.catchError(res, error)
  }
}

const _create = async (req, res, next) => {
  try {
    const checkDuplicateIds = req.body.garbages.filter((garbageId, index) => req.body.garbages.indexOf(garbageId) !== index)

    if (checkDuplicateIds.length > 0) return responseHandlers.badRequest(res, 'มี ID ซ้ำกันใน garbages')

    const garbageCategory = await GarbageCategoryModel.create({ ...req.body })
    await updateGarbages(garbageCategory, req.body.garbages)

    return responseHandlers.created(res, garbageCategory)
  } catch (error) {
    return responseHandlers.catchError(res, error)
  }
}

const _update = async (req, res, next) => {
  try {
    const checkDuplicateIds = req.body.garbages.filter((garbageId, index) => req.body.garbages.indexOf(garbageId) !== index)

    if (checkDuplicateIds.length > 0) return responseHandlers.badRequest(res, 'มี ID ซ้ำกันใน garbages')

    const garbageCategory = await GarbageCategoryModel.findOneAndUpdate(
      { _id: req.params.id },
      { $set: { ...req.body } },
      { new: true }
    )

    if (!garbageCategory) return responseHandlers.notFound(res, 'ไม่พบหมวดหมู่ของขยะ')

    await updateGarbages(garbageCategory, req.body.garbages)

    return responseHandlers.success(res, garbageCategory)
  } catch (error) {
    return responseHandlers.catchError(res, error)
  }
}

const _delete = async (req, res, next) => {
  try {
    await GarbageCategoryModel.findByIdAndDelete(req.params.id)

    return responseHandlers.success(res)
  } catch (error) {
    return responseHandlers.catchError(res, error)
  }
}

// Function
const updateGarbages = async (garbageCategory, garbageIds) => {
  // $addToSet จะตรวจสอบว่าค่าที่เราต้องการเพิ่มมีอยู่แล้วหรือไม่ หากไม่มีจะทำการเพิ่ม หากมีแล้วจะไม่เพิ่มซ้ำ
  const promises = garbageIds.map(async (garbageId) => {
    await GarbageModel.findByIdAndUpdate(
      garbageId,
      { $set: { garbage_category: garbageCategory._id } },
      { new: true }
    )
  })

  await Promise.all(promises)
}

module.exports = {
  _find,
  _findByID,
  _create,
  _update,
  _delete
}