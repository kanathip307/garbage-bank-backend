const { GarbageModel } = require('../../models/garbages')
const { GarbageCategoryModel } = require('../../models/garbages/category')
const responseHandlers = require('../utils/response')

const _find = async (req, res, next) => {
  try {
    const garbages = await GarbageModel.find().populate([
      { path: 'garbage_category', select: '-garbage_category -garbages' } // ใส่ขีด - ด้านหน้าฟิลด์ที่ไม่ต้องการให้ดึงมาด้วย
    ])

    return responseHandlers.success(res, garbages)
  } catch (error) {
    return responseHandlers.catchError(res, error)
  }
}

const _findByID = async (req, res, next) => {
  try {
    const garbage = await GarbageModel.findById(req.params.id).populate([
      { path: 'garbage_category', select: '-garbage_category -garbages' }
    ])

    if (!garbage) return responseHandlers.notFound(res, 'ไม่พบขยะ')

    return responseHandlers.success(res, garbage)
  } catch (error) {
    return responseHandlers.catchError(res, error)
  }
}

const _create = async (req, res, next) => {
  try {
    const garbage = await GarbageModel.create({ ...req.body })
    await updateReferences(req.body.garbage_category, GarbageCategoryModel, garbage._id)

    return responseHandlers.created(res, garbage)
  } catch (error) {
    return responseHandlers.catchError(res, error)
  }
}

const _update = async (req, res, next) => {
  try {
    const garbage = await GarbageModel.findOneAndUpdate(
      { _id: req.params.id },
      { $set: { ...req.body } },
      { new: true }
    )
    await updateReferences(req.body.garbage_category, GarbageCategoryModel, garbage._id)

    if (!garbage) return responseHandlers.notFound(res, 'ไม่พบขยะ')

    return responseHandlers.success(res, garbage)
  } catch (error) {
    return responseHandlers.catchError(res, error)
  }
}

const _delete = async (req, res, next) => {
  try {
    await GarbageModel.findByIdAndDelete(req.params.id)

    return responseHandlers.success(res)
  } catch (error) {
    return responseHandlers.catchError(res, error)
  }
}

// Function
const updateReferences = async (id, model, referenceId) => {
  await model.findByIdAndUpdate(
    id,
    { $addToSet: { garbages: referenceId } },
    { new: true }
  )
}

module.exports = {
  _find,
  _findByID,
  _create,
  _update,
  _delete
}
