const db = require('../config/db')

const PersonalAccessTokenSchema = db.Schema({
  user: { type: db.Schema.Types.ObjectId, required: true, ref: 'users' },
  token: { type: String, required: true },
  expired: { type: Boolean, required: true, default: false },
  createdAt: { type: Date, default: Date.now },
  __v: { type: Number, select: false }
})

const PersonalAccessTokenModel = db.model('personal_access_tokens', PersonalAccessTokenSchema)

module.exports = { PersonalAccessTokenModel }
