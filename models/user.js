const db = require('../config/db')

const UserSchema = db.Schema({
  first_name: { type: String, required: true },
  last_name: { type: String, required: true },
  image: { type: String, required: false, default: null },
  username: { type: String, required: true },
  password: { type: String, required: true },
  role: { type: Number, required: true, default: 1 },
  __v: { type: Number, select: false }
})

const UserModel = db.model('users', UserSchema)

module.exports = { UserModel }