const db = require('../config/db')

const NewsSchema = db.Schema({
  news_image: { type: String, required: true },
  news_src: { type: String, required: false },
  __v: { type: Number, select: false }
})

const NewsModel = db.model('news', NewsSchema)

module.exports = { NewsModel }