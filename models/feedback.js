const db = require('../config/db')

const FeedbackSchema = db.Schema({
  feedback_title: { type: String, required: true },
  feedback_details: { type: String, required: true },
  user: { type: db.Schema.Types.ObjectId, ref: 'users' },
  __v: { type: Number, select: false }
})

const FeedbackModel = db.model('feedback', FeedbackSchema)

module.exports = { FeedbackModel }
