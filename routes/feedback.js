const express = require('express')
const router = express.Router()
const feedbackController = require('../controllers/feedback')

router.route('/').get(feedbackController._find)
router.route('/:id').get(feedbackController._findByID)
router.route('/').post(feedbackController._create)
router.route('/:id').put(feedbackController._update)
router.route('/:id').delete(feedbackController._delete)

module.exports = router