const express = require('express')
const router = express.Router()
const garbageController = require('../../controllers/garbages')

router.route('/').get(garbageController._find)
router.route('/:id').get(garbageController._findByID)
router.route('/').post(garbageController._create)
router.route('/:id').put(garbageController._update)
router.route('/:id').delete(garbageController._delete)

module.exports = router