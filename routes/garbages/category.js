const express = require('express')
const router = express.Router()
const garbageCategoryController = require('../../controllers/garbages/category')

router.route('/').get(garbageCategoryController._find)
router.route('/:id').get(garbageCategoryController._findByID)
router.route('/').post(garbageCategoryController._create)
router.route('/:id').put(garbageCategoryController._update)
router.route('/:id').delete(garbageCategoryController._delete)

module.exports = router