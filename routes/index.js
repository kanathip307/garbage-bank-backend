const express = require('express')
const router = express.Router()

router.use('/auth', require('./users/auth'))
router.use('/users', require('./users/index'))
router.use('/news', require('./news'))
router.use('/garbages_category', require('./garbages/category'))
router.use('/garbages', require('./garbages'))
router.use('/feedback', require('./feedback'))
router.use('/transactionHistory', require('./transactionHistory'))

module.exports = router
